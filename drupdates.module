<?php

/**
 * @file
 * Drupdates module functions.
 */

module_load_include('inc', 'drupdates', 'includes/drupdates.project');
module_load_include('inc', 'drupdates', 'includes/drupdates.release');

/**
 * Implements hook_entity_info().
 */
function drupdates_entity_info() {
  $info = array();

  $info['drupdates_project'] = array(
    'label' => t('Projects'),
    'entity class' => 'DrupdatesProject',
    'controller class' => 'DrupdatesProjectController',
    'views controller class' => 'EntityDefaultViewsController',
    'base table' => 'drupdates_project',
    'revision table' => 'drupdates_project_revision',
    'fieldable' => TRUE,
    'entity keys' => array(
      'bundle' => 'bundle',
      'id' => 'dpid',
      'revision' => 'vid',
      'label' => 'project',
      'uuid' => 'uuid',
      'revision uuid' => 'vuuid',
    ),
    'bundle keys' => array('bundle' => 'bundle'),
    'bundles' => array(),
    'load hook' => 'drupdates_project_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Full Project'),
        'custom settings' => FALSE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'drupdates',
    'uuid' => TRUE,
    'access callback' => 'drupdates_project_access',
    'default_revision' => TRUE,
  );

  $info['drupdates_project_type'] = array(
    'label' => t('Project Type'),
    'entity class' => 'DrupdatesProjectType',
    'controller class' => 'DrupdatesProjectTypeControllerExportable',
    'base table' => 'drupdates_project_type',
    'fieldable' => FALSE,
    'bundle of' => 'drupdates_project',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'bundle',
      'name' => 'bundle',
      'label' => 'label',
      'status' => 'status',
      'module' => 'module',
    ),
    'module' => 'drupdates',
    'admin ui' => array(
      'path' => 'admin/structure/project-types',
      'file' => 'includes/drupdates.project.admin.inc',
    ),
    'access callback' => 'drupdates_project_access',
  );

  foreach (drupdates_project_type_load_bundles() as $name => $bundle) {
    $info['drupdates_project']['bundles'][$name] = array(
      'label' => $bundle->label,
      'admin' => array(
        'path' => 'admin/structure/project-types/manage/%',
        'real path' => 'admin/structure/project-types/manage/' . $name,
        'bundle argument' => 4,
        'access arguments' => array('administer project'),
      ),
    );
  }

  $info['drupdates_release'] = array(
    'label' => t('Releases'),
    'entity class' => 'DrupdatesRelease',
    'controller class' => 'DrupdatesReleaseController',
    'views controller class' => 'EntityDefaultViewsController',
    'base table' => 'drupdates_release',
    'revision table' => 'drupdates_release_revision',
    'fieldable' => TRUE,
    'entity keys' => array(
      'bundle' => 'bundle',
      'id' => 'drid',
      'revision' => 'vid',
      'uuid' => 'uuid',
      'revision uuid' => 'vuuid',
    ),
    'bundle keys' => array('bundle' => 'bundle'),
    'bundles' => array(),
    'load hook' => 'drupdates_release_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Full Release'),
        'custom settings' => FALSE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'drupdates',
    'uuid' => TRUE,
    'access callback' => 'drupdates_release_access',
    'default_revision' => TRUE,
  );

  $info['drupdates_release_type'] = array(
    'label' => t('Release Type'),
    'entity class' => 'DrupdatesReleaseType',
    'controller class' => 'DrupdatesReleaseTypeControllerExportable',
    'base table' => 'drupdates_release_type',
    'fieldable' => FALSE,
    'bundle of' => 'drupdates_release',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'bundle',
      'name' => 'bundle',
      'label' => 'label',
      'status' => 'status',
      'module' => 'module',
    ),
    'module' => 'drupdates',
    'admin ui' => array(
      'path' => 'admin/structure/release-types',
      'file' => 'includes/drupdates.release.admin.inc',
    ),
    'access callback' => 'drupdates_release_access',
  );

  foreach (drupdates_release_type_load_bundles() as $name => $bundle) {
    $info['drupdates_release']['bundles'][$name] = array(
      'label' => $bundle->label,
      'admin' => array(
        'path' => 'admin/structure/release-types/manage/%',
        'real path' => 'admin/structure/release-types/manage/' . $name,
        'bundle argument' => 4,
        'access arguments' => array('administer release'),
      ),
    );
  }

  return $info;
}

/**
 * Implements hook_entity_property_info().
 */
function drupdates_entity_property_info() {
  $info = array(
    'drupdates_project' => array(),
    'drupdates_release' => array(),
  );

  $info['drupdates_project']['properties'] = array(
    'dpid' => array(
      'label' => 'Project ID',
      'type' => 'integer',
      'description' => t('The unique project identifier.'),
      'schema field' => 'dpid',
    ),
    'vid' => array(
      'label' => 'Revision ID',
      'type' => 'integer',
      'description' => t('The unique project revision identifier.'),
      'schema field' => 'vid',
    ),
    'uuid' => array(
      'label' => 'UUID',
      'type' => 'text',
      'description' => t('The universally unique project identifier.'),
      'schema field' => 'uuid',
    ),
    'bundle' => array(
      'label' => 'Project type',
      'type' => 'token',
      'description' => t('The project type.'),
      'schema field' => 'bundle',
    ),
    'project' => array(
      'label' => 'Project name',
      'type' => 'text',
      'description' => t('The project name.'),
      'schema field' => 'project',
      'drupdates_extra_fields' => array('form'),
    ),
    'machine_name' => array(
      'label' => 'Machine name',
      'type' => 'machine_name',
      'description' => t('The project machine name.'),
      'schema field' => 'machine_name',
      'drupdates_extra_fields' => array('form'),
    ),
    'description' => array(
      'label' => 'Description',
      'type' => 'text',
      'description' => t('Short description of the project.'),
      'schema field' => 'description',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'description_format' => array(
      'label' => 'Description format',
      'type' => 'text',
      'description' => t('Format of project description.'),
      'schema field' => 'description_format',
      'drupdates_extra_fields' => array('form'),
    ),
    'active' => array(
      'label' => 'Is active',
      'type' => 'boolean',
      'description' => t('Is the project active?'),
      'schema field' => 'active',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'date',
      'description' => t('The date the project was created.'),
      'schema field' => 'created',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'created_uid' => array(
      'label' => 'Created by',
      'type' => 'user',
      'description' => t('The user who created the project.'),
      'schema field' => 'created_uid',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'modified' => array(
      'label' => 'Modified',
      'type' => 'date',
      'description' => t('The date the project was last modified.'),
      'schema field' => 'modified',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'modified_uid' => array(
      'label' => 'Modified by',
      'type' => 'user',
      'description' => t('The user who modified the project.'),
      'schema field' => 'modified_uid',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
  );

  $info['drupdates_release']['properties'] = array(
    'drid' => array(
      'label' => 'Release ID',
      'type' => 'integer',
      'description' => t('The unique release identifier.'),
      'schema field' => 'drid',
    ),
    'vid' => array(
      'label' => 'Revision ID',
      'type' => 'integer',
      'description' => t('The unique release revision identifier.'),
      'schema field' => 'vid',
    ),
    'uuid' => array(
      'label' => 'UUID',
      'type' => 'text',
      'description' => t('The universally unique release identifier.'),
      'schema field' => 'uuid',
    ),
    'bundle' => array(
      'label' => 'Release type',
      'type' => 'token',
      'description' => t('The release type.'),
      'schema field' => 'bundle',
    ),
    'core' => array(
      'label' => 'Core version',
      'type' => 'integer',
      'description' => t('The core version of the release.'),
      'schema field' => 'core',
      'drupdates_extra_fields' => array('form', 'display'),
      'edit' => TRUE,
    ),
    'version' => array(
      'label' => 'Version',
      'type' => 'decimal',
      'description' => t('The version number of the release.'),
      'schema field' => 'version',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'patch' => array(
      'label' => 'Patch level',
      'type' => 'text',
      'description' => t('The patch level for the release.'),
      'schema field' => 'patch',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'release_notes' => array(
      'label' => 'Release notes',
      'type' => 'text',
      'description' => t('Description of the release.'),
      'schema field' => 'release_notes',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'release_notes_format' => array(
      'label' => 'Release notes format',
      'type' => 'text',
      'description' => t('Description format of the release.'),
      'schema field' => 'release_notes_format',
      'drupdates_extra_fields' => array('form'),
    ),
    'dpid' => array(
      'label' => 'Project',
      'type' => 'drupdates_project',
      'description' => t('Reference to the project the release relates to.'),
      'schema field' => 'dpid',
    ),
    'active' => array(
      'label' => 'Is active',
      'type' => 'boolean',
      'description' => t('Is the release active?'),
      'schema field' => 'active',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'security' => array(
      'label' => 'Security release',
      'type' => 'boolean',
      'description' => t('Does the release contain security fixes?'),
      'schema field' => 'security',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'targz_fid' => array(
      'label' => 'Tar.gz file',
      'type' => 'file',
      'description' => t('The targz file for release.'),
      'schema field' => 'targz_fid',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'targz_md5' => array(
      'label' => 'Tar.gz md5 file',
      'type' => 'text',
      'description' => t('The targz md5 file for release.'),
      'schema field' => 'targz_md5',
    ),
    'zip_fid' => array(
      'label' => 'Zip file',
      'type' => 'file',
      'description' => t('The zip file for release.'),
      'schema field' => 'zip_fid',
      'drupdates_extra_fields' => array('form', 'display'),
    ),
    'zip_md5' => array(
      'label' => 'Zip md5 file',
      'type' => 'text',
      'description' => t('The zip md5 file for release.'),
      'schema field' => 'zip_md5',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'date',
      'description' => t('The Unix timestamp when the release was created.'),
      'schema field' => 'created',
      'drupdates_extra_fields' => array('display'),
    ),
    'created_uid' => array(
      'label' => 'Created by',
      'type' => 'user',
      'description' => t('The user who created the release.'),
      'schema field' => 'created_uid',
      'drupdates_extra_fields' => array('display'),
    ),
    'modified' => array(
      'label' => 'Modified',
      'type' => 'date',
      'description' => t('The Unix timestamp when the release was most recently modified.'),
      'schema field' => 'modified',
      'drupdates_extra_fields' => array('display'),
    ),
    'modified_uid' => array(
      'label' => 'Modified by',
      'type' => 'user',
      'description' => t('The user who made the most recent change.'),
      'schema field' => 'modified_uid',
      'drupdates_extra_fields' => array('display'),
    ),
  );

  return $info;
}

/**
 * Implements hook_help().
 */
function drupdates_help($path, $args) {
  switch ($path) {
    case 'admin/help#drupdates':
      return '<p>' . t('To deploy a project, follow these five steps:') . '</p>'
        . '<ol>'
        . '<li>' . t('Create a new project and make sure it has a unique short name.') . '</li>'
        . '<li>' . t('Add the following lines to the .info file of your project:')
        . '<ul>'
        . '<li>' . t('project = Your project short name') . '</li>'
        . '<li>' . t('project status url = http://drupdates.example.com/release-history') . '</li>'
        . '</ul></li>'
        . '<li>' . t('Package your project as a tar.gz-file and zip-file. Preferably use the short name and version as file name.') . '</li>'
        . '<li>' . t('Now add a release to the project by uploading the tar.gz and zip.')
        . '<li>' . t('You\'re done. You can now download the module from your Drupal website and it will check for updates on your private server!') . '</li>'
        . '</ul>';
  }
}

/**
 * Implements hook_permission().
 */
function drupdates_permission() {
  $perms = array();

  $perms['administer project'] = array(
    'title' => t('Administer Project'),
    'description' => t('Handle the entire project entities.'),
  );

  $perms['configure project'] = array(
    'title' => t('Configure Project Types'),
    'description' => t('Able to manage the bundle types for projects.'),
  );

  $project_actions = array('create', 'edit', 'view', 'delete');

  foreach ($project_actions as $project_action) {
    $perms["$project_action any project"] = array(
      'title' => t(
        '%action any project', array(
          '%action' => ucfirst($project_action),
        )
      ),
    );
  }

  foreach (drupdates_project_type_load_bundles() as $name => $bundle) {
    foreach ($project_actions as $project_action) {
      $perms["$project_action $name project"] = array(
        'title' => t(
          '%action %name project', array(
            '%action' => ucfirst($project_action),
            '%name' => $bundle->bundle,
          )
        ),
      );
    }
  }

  $perms['administer release'] = array(
    'title' => t('Administer Release'),
    'description' => t('Handle the entire release entities.'),
  );

  $perms['view release history'] = array(
    'title' => t('View release history'),
    'description' => t('Get release information as XML.'),
  );

  $release_actions = array('create', 'edit', 'view', 'delete');

  foreach (drupdates_release_type_load_bundles() as $name => $bundle) {
    foreach ($release_actions as $release_action) {
      $perms["$release_action $name release"] = array(
        'title' => t(
          '%action %name release', array(
            '%action' => ucfirst($release_action),
            '%name' => $bundle->bundle,
          )
        ),
      );

      $perms["$release_action any release"] = array(
        'title' => t(
          '%action any release', array(
            '%action' => ucfirst($release_action),
          )
        ),
      );
    }
  }

  return $perms;
}

/**
 * Implements hook_menu().
 */
function drupdates_menu() {
  $items = array();

  $items['project/%drupdates_project'] = array(
    'title callback' => 'drupdates_project_page_title',
    'title arguments' => array(1),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'drupdates_project', 1),
    'page callback' => 'drupdates_project_view',
    'page arguments' => array(1),
  );

  $items['project/%drupdates_project/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['project/%drupdates_project/edit'] = array(
    'title' => 'Edit',
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'drupdates_project', 1),
    'page callback' => 'drupdates_project_edit',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['project/%drupdates_project/delete'] = array(
    'title' => 'Delete',
    'access callback' => 'entity_access',
    'access arguments' => array('delete', 'drupdates_project', 1),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drupdates_project_delete_form', 1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  $items['project/add'] = array(
    'title' => 'Add new project',
    'access callback' => 'entity_access',
    'access arguments' => array('create', 'drupdates_project'),
    'page callback' => 'drupdates_project_page_add',
  );

  foreach (drupdates_project_type_load_bundles() as $name => $bundle) {
    $items['project/add/' . $name] = array(
      'title' => 'Add new project',
      'page callback' => 'drupdates_project_add',
      'page arguments' => array(2),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'drupdates_project', $bundle),
    );
  }

  $items['project/%drupdates_project/release/%drupdates_release'] = array(
    'title callback' => 'drupdates_release_page_title',
    'title arguments' => array(3),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'drupdates_release', 3),
    'page callback' => 'drupdates_release_view',
    'page arguments' => array(3),
  );

  $items['project/%drupdates_project/release/%drupdates_release/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['project/%drupdates_project/release/%drupdates_release/edit'] = array(
    'title' => 'Edit',
    'access callback' => 'entity_access',
    'access arguments' => array('edit', 'drupdates_release', 3),
    'page callback' => 'drupdates_release_edit',
    'page arguments' => array(3),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['project/%drupdates_project/release/%drupdates_release/delete'] = array(
    'title' => 'Delete',
    'access callback' => 'entity_access',
    'access arguments' => array('delete', 'drupdates_release', 3),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drupdates_release_delete_form', 3),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  $items['release/add'] = array(
    'title' => 'Add new release',
    'access callback' => 'entity_access',
    'access arguments' => array('create', 'drupdates_release'),
    'page callback' => 'drupdates_release_page_add',
  );

  foreach (drupdates_release_type_load_bundles() as $name => $bundle) {
    $items['release/add/' . $name] = array(
      'title' => 'Add new release',
      'page callback' => 'drupdates_release_add',
      'page arguments' => array(2),
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'drupdates_release', $bundle),
    );
  }

  $items['release-history/%/%'] = array(
    'page callback' => 'drupdates_release_history',
    'page arguments' => array(1, 2),
    'access arguments' => array('view release history'),
    'file' => 'includes/drupdates.release.history.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/structure/release-types/manage/%/property/%'] = array(
    'title callback' => 'drupdates_release_title_edit_property',
    'title arguments' => array(4, 6),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drupdates_release_property_edit_form', 4, 6),
    'access arguments' => array('edit', 'drupdates_release', 4),
    'access callback' => 'entity_access',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function drupdates_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link on 'admin/content/project' page.
  if ($root_path == 'admin/content/project') {
    $item = menu_get_item('project/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
  // Add action link on 'admin/content/release' page.
  elseif ($root_path == 'admin/content/release') {
    $item = menu_get_item('release/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
  elseif ($root_path == 'project/%') {
    $item = menu_get_item('release/add');
    $item['href'] = url("release/add/release", array(
        'query' => array('dpid' => arg(1)),
        'absolute' => TRUE,
      )
    );
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}

/**
 * Implements hook_menu_alter().
 */
function drupdates_menu_alter(&$items) {
  $items['admin/content/project']['type'] = MENU_NORMAL_ITEM | MENU_LOCAL_TASK;
  $items['admin/content/release']['type'] = MENU_NORMAL_ITEM | MENU_LOCAL_TASK;
}

/**
 * Implements hook_field_extra_fields().
 */
function drupdates_field_extra_fields() {
  return array_merge(
    drupdates_extra_fields('drupdates_project'),
    drupdates_extra_fields('drupdates_release')
  );
}

/**
 * Retrieve the structure for hook_field_extra_fields().
 */
function drupdates_extra_fields($entity_type) {
  $entity = entity_get_property_info($entity_type);

  $extra_fields = array();
  $weight = -50;

  foreach ($entity['properties'] as $field_name => $field_properties) {
    if (isset($field_properties['drupdates_extra_fields']) && is_array($field_properties['drupdates_extra_fields'])) {
      foreach ($field_properties['drupdates_extra_fields'] as $value) {
        if (!isset($extra_fields[$value])) {
          $extra_fields[$value] = array();
        }
        $extra_fields[$value][$field_name] = array(
          'label' => $field_properties['label'],
          'description' => $field_properties['description'],
          'weight' => $weight,
        );

        // Exception for editables properties.
        if (isset($field_properties['edit']) && $value == 'form') {
          $extra_fields[$value][$field_name]['edit'] = TRUE;
        }
      }
    }
    ++$weight;
  }

  $entities = array();
  $entity_info = entity_get_info($entity_type);
  foreach (array_keys($entity_info['bundles']) as $bundle) {
    foreach ($extra_fields['form'] as $property_name => $property_values) {
      if (isset($property_values['edit'])) {
        $path = "admin/structure/release-types/manage/$bundle/property/$property_name";
        $extra_fields['form'][$property_name]['edit'] = l(t('edit'), $path, array('attributes' => array('title' => 'edit')));
      }
    }

    $entities[$bundle] = $extra_fields;
  }

  return array($entity_type => $entities);
}

/**
 * Implements hook_theme().
 */
function drupdates_theme() {
  $themes = array();

  $themes['drupdates_project_add_list'] = array(
    'file' => 'includes/drupdates.project.theme.inc',
    'variables' => array('content' => NULL),
  );

  $themes['drupdates_release_add_list'] = array(
    'file' => 'includes/drupdates.release.theme.inc',
    'variables' => array('content' => NULL),
  );

  return $themes;
}

/**
 * Implements hook_views_api().
 */
function drupdates_views_api() {
  return array('api' => 3);
}

/**
 * Implements hook_file_download().
 */
function drupdates_file_download($uri) {
  $files = entity_load('file', FALSE, array('uri' => $uri));

  if (empty($files)) {
    return NULL;
  }

  $file = reset($files);

  foreach (array('targz_fid', 'zip_fid') as $property) {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'drupdates_release')
       ->propertyCondition($property, $file->fid)
       ->execute();

    if (empty($result)) {
      return NULL;
    }

    $release = entity_load_single('drupdates_release', array_keys($result['drupdates_release']));
    if (!$release) {
      return NULL;
    }

    global $user;
    $has_access = entity_access('view', 'drupdates_release', $release, $user);

    if (!$has_access) {
      return -1;
    }

    $headers = file_get_content_headers($file);
    return $headers;
  }
}
