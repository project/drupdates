<?php

/**
 * @file
 * drupdates.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupdates_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'drupdates_project_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'drupdates_project';
  $view->human_name = 'Projects List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Project List';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view any release';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'dpid' => 'dpid',
    'project' => 'project',
    'active' => 'active',
    'dpid_1' => 'dpid_1',
    'dpid_2' => 'dpid_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'dpid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'project' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'active' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dpid_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => ' | ',
      'empty_column' => 0,
    ),
    'dpid_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Drupdates Project: Project ID */
  $handler->display->display_options['fields']['dpid']['id'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['dpid']['field'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['separator'] = '';
  /* Field: Drupdates Project: Project name */
  $handler->display->display_options['fields']['project']['id'] = 'project';
  $handler->display->display_options['fields']['project']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['project']['field'] = 'project';
  $handler->display->display_options['fields']['project']['label'] = 'Project Name';
  $handler->display->display_options['fields']['project']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['project']['alter']['path'] = 'project/[dpid]';
  /* Field: Drupdates Project: Is active */
  $handler->display->display_options['fields']['active']['id'] = 'active';
  $handler->display->display_options['fields']['active']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['active']['field'] = 'active';
  $handler->display->display_options['fields']['active']['label'] = 'Active';
  $handler->display->display_options['fields']['active']['not'] = 0;
  /* Field: Drupdates Project: Project ID */
  $handler->display->display_options['fields']['dpid_1']['id'] = 'dpid_1';
  $handler->display->display_options['fields']['dpid_1']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['dpid_1']['field'] = 'dpid';
  $handler->display->display_options['fields']['dpid_1']['label'] = 'Operations';
  $handler->display->display_options['fields']['dpid_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['dpid_1']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['dpid_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['dpid_1']['alter']['path'] = 'project/[dpid]/edit';
  $handler->display->display_options['fields']['dpid_1']['separator'] = '';
  /* Field: Drupdates Project: Project ID */
  $handler->display->display_options['fields']['dpid_2']['id'] = 'dpid_2';
  $handler->display->display_options['fields']['dpid_2']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['dpid_2']['field'] = 'dpid';
  $handler->display->display_options['fields']['dpid_2']['label'] = '';
  $handler->display->display_options['fields']['dpid_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['dpid_2']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['dpid_2']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['dpid_2']['alter']['path'] = 'project/[dpid]/delete';
  $handler->display->display_options['fields']['dpid_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dpid_2']['separator'] = '';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/project';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Projects';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['drupdates_project_list'] = $view;

  $view = new view();
  $view->name = 'drupdates_release_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'drupdates_release';
  $view->human_name = 'Releases List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Release List';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view any release';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'drid' => 'drid',
    'dpid' => 'dpid',
    'project' => 'project',
    'core' => 'core',
    'version' => 'version',
    'patch' => 'patch',
    'nothing' => 'nothing',
    'filename' => 'filename',
    'filename_1' => 'filename_1',
    'nothing_1' => 'nothing_1',
    'nothing_2' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'drid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dpid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'project' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'core' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'version' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'patch' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'filename' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'filename_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => ' | ',
      'empty_column' => 0,
    ),
    'nothing_2' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Releases: Project */
  $handler->display->display_options['relationships']['dpid']['id'] = 'dpid';
  $handler->display->display_options['relationships']['dpid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['dpid']['field'] = 'dpid';
  $handler->display->display_options['relationships']['dpid']['required'] = TRUE;
  /* Relationship: Releases: Tar.gz file */
  $handler->display->display_options['relationships']['targz_fid']['id'] = 'targz_fid';
  $handler->display->display_options['relationships']['targz_fid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['targz_fid']['field'] = 'targz_fid';
  $handler->display->display_options['relationships']['targz_fid']['label'] = 'Targz File';
  /* Relationship: Releases: Zip file */
  $handler->display->display_options['relationships']['zip_fid']['id'] = 'zip_fid';
  $handler->display->display_options['relationships']['zip_fid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['zip_fid']['field'] = 'zip_fid';
  $handler->display->display_options['relationships']['zip_fid']['label'] = 'Zip File';
  /* Field: Releases: Release ID */
  $handler->display->display_options['fields']['drid']['id'] = 'drid';
  $handler->display->display_options['fields']['drid']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['drid']['field'] = 'drid';
  $handler->display->display_options['fields']['drid']['label'] = '';
  $handler->display->display_options['fields']['drid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['drid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['drid']['separator'] = '';
  /* Field: Releases: Project */
  $handler->display->display_options['fields']['dpid']['id'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['dpid']['field'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['label'] = '';
  $handler->display->display_options['fields']['dpid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['dpid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dpid']['separator'] = '';
  /* Field: Projects: Project name */
  $handler->display->display_options['fields']['project']['id'] = 'project';
  $handler->display->display_options['fields']['project']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['project']['field'] = 'project';
  $handler->display->display_options['fields']['project']['relationship'] = 'dpid';
  $handler->display->display_options['fields']['project']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['project']['alter']['path'] = 'project/[dpid]';
  /* Field: Releases: Core version */
  $handler->display->display_options['fields']['core']['id'] = 'core';
  $handler->display->display_options['fields']['core']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['core']['field'] = 'core';
  $handler->display->display_options['fields']['core']['label'] = '';
  $handler->display->display_options['fields']['core']['exclude'] = TRUE;
  $handler->display->display_options['fields']['core']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['core']['separator'] = '';
  /* Field: Releases: Version */
  $handler->display->display_options['fields']['version']['id'] = 'version';
  $handler->display->display_options['fields']['version']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['version']['field'] = 'version';
  $handler->display->display_options['fields']['version']['label'] = '';
  $handler->display->display_options['fields']['version']['exclude'] = TRUE;
  $handler->display->display_options['fields']['version']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['version']['precision'] = '0';
  $handler->display->display_options['fields']['version']['separator'] = '.';
  /* Field: Releases: Patch level */
  $handler->display->display_options['fields']['patch']['id'] = 'patch';
  $handler->display->display_options['fields']['patch']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['patch']['field'] = 'patch';
  $handler->display->display_options['fields']['patch']['label'] = '';
  $handler->display->display_options['fields']['patch']['exclude'] = TRUE;
  $handler->display->display_options['fields']['patch']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Version';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[core].x-[version].[patch]';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'project/[dpid]/release/[drid]';
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename']['id'] = 'filename';
  $handler->display->display_options['fields']['filename']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename']['field'] = 'filename';
  $handler->display->display_options['fields']['filename']['relationship'] = 'targz_fid';
  $handler->display->display_options['fields']['filename']['label'] = 'Tarball';
  $handler->display->display_options['fields']['filename']['link_to_file'] = TRUE;
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename_1']['id'] = 'filename_1';
  $handler->display->display_options['fields']['filename_1']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename_1']['field'] = 'filename';
  $handler->display->display_options['fields']['filename_1']['relationship'] = 'zip_fid';
  $handler->display->display_options['fields']['filename_1']['label'] = 'Zip';
  $handler->display->display_options['fields']['filename_1']['link_to_file'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'project/[dpid]/release/[drid]/edit';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['label'] = '';
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['nothing_2']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['path'] = 'project/[dpid]/release/[drid]/delete';
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/content/release';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Releases';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['drupdates_release_list'] = $view;

  $view = new view();
  $view->name = 'drupdates_related_release_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'drupdates_release';
  $view->human_name = 'Drupdates Related Release List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Related Release List';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view any release';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'drid' => 'drid',
    'dpid' => 'dpid',
    'project' => 'project',
    'core' => 'core',
    'version' => 'version',
    'patch' => 'patch',
    'nothing' => 'nothing',
    'filename' => 'filename',
    'filename_1' => 'filename_1',
    'nothing_1' => 'nothing_1',
    'nothing_2' => 'nothing_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'drid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dpid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'project' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'core' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'version' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'patch' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'filename' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'filename_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing_1' => array(
      'align' => '',
      'separator' => ' | ',
      'empty_column' => 0,
    ),
    'nothing_2' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Releases: Project */
  $handler->display->display_options['relationships']['dpid']['id'] = 'dpid';
  $handler->display->display_options['relationships']['dpid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['dpid']['field'] = 'dpid';
  $handler->display->display_options['relationships']['dpid']['required'] = TRUE;
  /* Relationship: Releases: Tar.gz file */
  $handler->display->display_options['relationships']['targz_fid']['id'] = 'targz_fid';
  $handler->display->display_options['relationships']['targz_fid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['targz_fid']['field'] = 'targz_fid';
  $handler->display->display_options['relationships']['targz_fid']['label'] = 'Targz File';
  /* Relationship: Releases: Zip file */
  $handler->display->display_options['relationships']['zip_fid']['id'] = 'zip_fid';
  $handler->display->display_options['relationships']['zip_fid']['table'] = 'drupdates_release';
  $handler->display->display_options['relationships']['zip_fid']['field'] = 'zip_fid';
  $handler->display->display_options['relationships']['zip_fid']['label'] = 'Zip File';
  /* Field: Releases: Release ID */
  $handler->display->display_options['fields']['drid']['id'] = 'drid';
  $handler->display->display_options['fields']['drid']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['drid']['field'] = 'drid';
  $handler->display->display_options['fields']['drid']['label'] = '';
  $handler->display->display_options['fields']['drid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['drid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['drid']['separator'] = '';
  /* Field: Releases: Project */
  $handler->display->display_options['fields']['dpid']['id'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['dpid']['field'] = 'dpid';
  $handler->display->display_options['fields']['dpid']['label'] = '';
  $handler->display->display_options['fields']['dpid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['dpid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['dpid']['alter']['path'] = 'project/[dpid]';
  $handler->display->display_options['fields']['dpid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['dpid']['separator'] = '';
  /* Field: Projects: Project name */
  $handler->display->display_options['fields']['project']['id'] = 'project';
  $handler->display->display_options['fields']['project']['table'] = 'drupdates_project';
  $handler->display->display_options['fields']['project']['field'] = 'project';
  $handler->display->display_options['fields']['project']['relationship'] = 'dpid';
  $handler->display->display_options['fields']['project']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['project']['alter']['path'] = 'project/[dpid]';
  /* Field: Releases: Core version */
  $handler->display->display_options['fields']['core']['id'] = 'core';
  $handler->display->display_options['fields']['core']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['core']['field'] = 'core';
  $handler->display->display_options['fields']['core']['label'] = '';
  $handler->display->display_options['fields']['core']['exclude'] = TRUE;
  $handler->display->display_options['fields']['core']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['core']['separator'] = '';
  /* Field: Releases: Version */
  $handler->display->display_options['fields']['version']['id'] = 'version';
  $handler->display->display_options['fields']['version']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['version']['field'] = 'version';
  $handler->display->display_options['fields']['version']['label'] = '';
  $handler->display->display_options['fields']['version']['exclude'] = TRUE;
  $handler->display->display_options['fields']['version']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['version']['precision'] = '0';
  $handler->display->display_options['fields']['version']['separator'] = '.';
  /* Field: Releases: Patch level */
  $handler->display->display_options['fields']['patch']['id'] = 'patch';
  $handler->display->display_options['fields']['patch']['table'] = 'drupdates_release';
  $handler->display->display_options['fields']['patch']['field'] = 'patch';
  $handler->display->display_options['fields']['patch']['label'] = '';
  $handler->display->display_options['fields']['patch']['exclude'] = TRUE;
  $handler->display->display_options['fields']['patch']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Version';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[core].x-[version].[patch]';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'project/[dpid]/release/[drid]';
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename']['id'] = 'filename';
  $handler->display->display_options['fields']['filename']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename']['field'] = 'filename';
  $handler->display->display_options['fields']['filename']['relationship'] = 'targz_fid';
  $handler->display->display_options['fields']['filename']['label'] = 'Tarball';
  $handler->display->display_options['fields']['filename']['link_to_file'] = TRUE;
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename_1']['id'] = 'filename_1';
  $handler->display->display_options['fields']['filename_1']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename_1']['field'] = 'filename';
  $handler->display->display_options['fields']['filename_1']['relationship'] = 'zip_fid';
  $handler->display->display_options['fields']['filename_1']['label'] = 'Zip';
  $handler->display->display_options['fields']['filename_1']['link_to_file'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'project/[dpid]/release/[drid]/edit';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['label'] = '';
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['nothing_2']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['path'] = 'project/[dpid]/release/[drid]/delete';
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;
  /* Contextual filter: Releases: Project */
  $handler->display->display_options['arguments']['dpid']['id'] = 'dpid';
  $handler->display->display_options['arguments']['dpid']['table'] = 'drupdates_release';
  $handler->display->display_options['arguments']['dpid']['field'] = 'dpid';
  $handler->display->display_options['arguments']['dpid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['dpid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['dpid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['dpid']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'project/%/releases';
  $export['drupdates_related_release_list'] = $view;

  return $export;
}
