<?php

/**
 * @file
 * Theme hook implementations for drupdates_release entity.
 */

/**
 * Returns HTML for a list of available release types for creation.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of release types.
 *
 * @ingroup themeable
 */
function theme_drupdates_release_add_list($variables) {
  $items = array();

  foreach ($variables['content'] as $item) {
    $items[] = array(
      'data' => l($item->label, 'release/add/' . $item->bundle),
    );
  }

  $output = array(
    'items' => $items,
    'title' => 'Release types',
    'type' => 'ul',
    'attributes' => array('class' => 'release-type-list admin-list'),
  );

  return theme_item_list($output);
}
