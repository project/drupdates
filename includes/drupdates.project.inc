<?php

/**
 * @file
 * Drupdates project functions.
 */

/**
 * Access callback for the entity API.
 *
 * @see entity_access()
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param object $entity
 *   (optional) A type to check access for. If nothing is given, access for
 *   all profiles is determined.
 * @param object $account
 *   (optional) The user to check for.
 *   Leave it to NULL to check for the global user.
 * @param string $entity_type
 *   (optional) Leave it to NULL to get the property from $entity.
 *
 * @return boolean
 *   Whether access is allowed or not.
 */
function drupdates_project_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {

  if (user_access('administer project', $account) || user_access('configure project', $account)) {
    return TRUE;
  }

  $bundle = (!empty($entity->bundle)) ? $entity->bundle : '';

  $permissions = array(
    "{$op} {$bundle} project",
    "{$op} any project",
  );

  foreach ($permissions as $permission) {
    if (user_access($permission, $account)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Page view callback.
 */
function drupdates_project_view(DrupdatesProject $drupdates_project, $view_mode = 'full') {
  drupal_set_title(drupdates_project_page_title($drupdates_project));

  $entity_id = array(entity_id('drupdates_project', $drupdates_project) => $drupdates_project);

  $entity = entity_view('drupdates_project', $entity_id, $view_mode);

  $releases = views_embed_view('drupdates_related_release_list', 'page', $drupdates_project->dpid);

  return drupal_render($entity) . $releases;
}

/**
 * Generate the page title.
 */
function drupdates_project_page_title(DrupdatesProject $drupdates_project) {
  return t('Project: @project', array('@project' => $drupdates_project->project));
}

/**
 * Render the create drupdates project.
 */
function drupdates_project_add($bundle) {
  $args = array('bundle' => $bundle);

  $drupdates_project = entity_create('drupdates_project', $args);

  return drupal_get_form('drupdates_project_edit_form', $drupdates_project);
}

/**
 * Display the edit form.
 */
function drupdates_project_edit(DrupdatesProject $drupdates_project) {
  return drupal_get_form('drupdates_project_edit_form', $drupdates_project);
}

/**
 * Generate the edit form.
 */
function drupdates_project_edit_form($form, $form_state, $drupdates_project) {
  $form['bundle'] = array(
    '#type' => 'value',
    '#value' => $drupdates_project->bundle,
  );

  $form['default_revision'] = array(
    '#type' => 'value',
    '#value' => $drupdates_project->default_revision,
  );

  if (!empty($drupdates_project->dpid)) {
    $form['dpid'] = array(
      '#type' => 'value',
      '#value' => $drupdates_project->dpid,
    );
    $form['vid'] = array(
      '#type' => 'value',
      '#value' => $drupdates_project->vid,
    );
  }
  else {
    $form['is_new'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );
  }

  $form['project'] = array(
    '#title' => t('Project Name'),
    '#type' => 'textfield',
    '#default_value' => $drupdates_project->project,
    '#required' => TRUE,
    '#maxlength' => 100,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($drupdates_project->machine_name) ? $drupdates_project->machine_name : '',
    '#disabled' => empty($drupdates_project->is_new),
    '#machine_name' => array(
      'exists' => 'drupdates_project_exists',
      'source' => array('project'),
    ),
  );

  $form['description'] = array(
    '#type' => 'text_format',
    '#title' => t('Description'),
    '#default_value' => $drupdates_project->description,
    '#format' => $drupdates_project->description_format,
    '#required' => TRUE,
  );

  $form['active'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#default_value' => $drupdates_project->active,
    '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
    '#required' => TRUE,
  );

  field_attach_form('drupdates_project', $drupdates_project, $form, $form_state);

  $form['actions'] = array(
    '#weight' => 1000,
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  if (!empty($drupdates_project->dpid)) {
    if (user_access("delete $drupdates_project->bundle project") || user_access("delete any project")) {
      $entity_uri = entity_uri('drupdates_project', $drupdates_project);
      $form['actions']['delete'] = array(
        '#type' => 'link',
        '#title' => t('Delete'),
        '#href' => $entity_uri['path'] . '/delete',
      );
    }

    foreach (drupdates_release_load_multiple() as $release_bundle) {
      if (user_access("create $release_bundle->bundle release") || user_access("create any release")) {
        $form['actions']['create_release'] = array(
          '#type' => 'link',
          '#title' => t('Add new release'),
          '#href' => url("release/add/$release_bundle->bundle", array(
            'query' => array('dpid' => $drupdates_project->dpid),
            'absolute' => TRUE)
          ),
        );
      }
    }
  }

  return $form;
}

/**
 * Attach the form validation on project form.
 */
function drupdates_project_edit_form_validate($form, &$form_state) {
  $drupdates_project = (object) $form_state['values'];
  field_attach_form_validate('drupdates_project', $drupdates_project, $form, $form_state);
}

/**
 * Handles submission of the project edit form.
 */
function drupdates_project_edit_form_submit($form, &$form_state) {
  $drupdates_project = (object) $form_state['values'];
  field_attach_submit('drupdates_project', $drupdates_project, $form, $form_state);
  $saved_project = drupdates_project_save($drupdates_project);
  $form_state['redirect'] = "project/$saved_project->dpid";
}

/**
 * Saves an drupdates project.
 */
function drupdates_project_save($drupdates_project) {
  global $user;

  $description = $drupdates_project->description;
  $drupdates_project->description = $description['value'];
  $drupdates_project->description_format = $description['format'];

  $drupdates_project->modified_uid = $user->uid;
  $drupdates_project->modified = REQUEST_TIME;
  if (!empty($drupdates_project->is_new)) {
    $drupdates_project->created = REQUEST_TIME;
    $drupdates_project->created_uid = $user->uid;
  }
  else {
    $drupdates_project->revision = TRUE;
  }

  entity_save('drupdates_project', $drupdates_project);

  return $drupdates_project;
}

/**
 * Form for deleting drupdates project.
 */
function drupdates_project_delete_form($form, $form_state, $drupdates_project) {
  $entity_uri = entity_uri('drupdates_project', $drupdates_project);
  $title = entity_label('drupdates_project', $drupdates_project);
  return confirm_form(
    $form,
    t("Are you sure you want to delete <strong>@title</strong>?", array('@title' => $title)),
    $entity_uri['path'] . '/view',
    t("Deleting this project will trigger the deletion of all associated resources and cannot be undone."),
    t("Delete")
  );
}

/**
 * Submit handler drupdates_project_delete_form.
 */
function drupdates_project_delete_form_submit($form, &$form_state) {
  $drupdates_project = reset($form_state['build_info']['args']);
  $title = entity_label('drupdates_project', $drupdates_project);
  entity_delete('drupdates_project', $drupdates_project->dpid);
  drupdates_project_delete_related_releases($drupdates_project->dpid);
  drupal_set_message(t("Project '@title' was successfully deleted.", array('@title' => $title)));
  $form_state['redirect'] = 'admin/content/project';
}

/**
 * Remove the project related releases.
 *
 * @param int $dpid
 *   The drupdates project id.
 */
function drupdates_project_delete_related_releases($dpid) {
  $releases = entity_load('drupdates_release', FALSE, array('dpid' => $dpid));

  foreach ($releases as $drupdates_release) {
    drupdates_release_delete($drupdates_release);
  }
}

/**
 * Load drupdates project object.
 */
function drupdates_project_load($dpid, $reset = FALSE) {
  return entity_load_single('drupdates_project', $dpid);
}

/**
 * Loads one or more drupdates project objects.
 */
function drupdates_project_load_multiple($dpid = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('drupdates_project', $dpid, $conditions, $reset);
}

/**
 * Checks if a project exists.
 *
 * @param text $name
 *   The machine name to look up.
 *
 * @return bool
 *   Does the project exist?
 */
function drupdates_project_exists($name) {
  $project = drupdates_project_load_multiple(FALSE, array('machine_name' => $name));
  return !empty($project);
}

/**
 * List all drupdates project objects.
 *
 * @param bool $only_active
 *   Set TRUE to return only the activated projects, FALSE otherwise.
 */
function drupdates_project_list($only_active = FALSE) {
  $conditions = array();
  if ($only_active) {
    $conditions = array('active' => TRUE);
  }

  $drupdates_projects = drupdates_project_load_multiple(FALSE, $conditions);
  foreach ($drupdates_projects as &$drupdates_project) {
    $drupdates_project = $drupdates_project->project;
  }
  return $drupdates_projects;
}

/**
 * Checks if a project type bundle exists.
 *
 * @param text $name
 *   The bundle to look up.
 *
 * @return bool
 *   Does the bundle exist?
 */
function drupdates_project_type_bundle_exists($name) {
  $bundles = drupdates_project_type_load_bundles();
  return isset($bundles[$name]);
}

/**
 * Returns all defined project type bundles.
 *
 * @return array
 *   All project type bundles.
 */
function drupdates_project_type_load_bundles() {
  $bundle = &drupal_static(__FUNCTION__);

  if (!isset($bundle)) {
    $bundle = db_select('drupdates_project_type', 'dpt')
      ->fields('dpt')
      ->orderBy('bundle')
      ->execute()
      ->fetchAllAssoc('bundle');
  }

  return $bundle;
}

/**
 * List drupdates project types that can be created.
 */
function drupdates_project_page_add() {
  $bundles = drupdates_project_type_load_bundles();

  switch (count($bundles)) {
    case 1:
      $bundle = array_shift($bundles);
      drupal_goto("project/add/{$bundle->bundle}");
      break;

    case 0:
      return '<p>' . t('You have not created any project types yet.') . '</p>';

    default:
      return theme('drupdates_project_add_list', array('content' => $bundles));
  }
}
