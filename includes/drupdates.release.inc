<?php

/**
 * @file
 * Drupdates release functions.
 */

/**
 * Access callback for the entity API.
 *
 * @see entity_access()
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param object $entity
 *   (optional) A type to check access for. If nothing is given, access for
 *   all profiles is determined.
 * @param object $account
 *   (optional) The user to check for.
 *   Leave it to NULL to check for the global user.
 * @param string $entity_type
 *   (optional) Leave it to NULL to get the property from $entity.
 *
 * @return boolean
 *   Whether access is allowed or not.
 */
function drupdates_release_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {

  if (user_access('administer release', $account)) {
    return TRUE;
  }

  $bundle = (!empty($entity->bundle)) ? $entity->bundle : '';

  $permissions = array(
    "{$op} {$bundle} release",
    "{$op} any release",
  );

  foreach ($permissions as $permission) {
    if (user_access($permission, $account)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Page view callback.
 */
function drupdates_release_view(DrupdatesRelease $drupdates_release, $view_mode = 'full') {
  drupal_set_title(drupdates_release_page_title($drupdates_release));
  return entity_view('drupdates_release', array(entity_id('drupdates_release', $drupdates_release) => $drupdates_release), $view_mode);
}

/**
 * Generate the page title.
 */
function drupdates_release_page_title(DrupdatesRelease $release) {
  $title = "{$release->core}.x-{$release->version}.{$release->patch}";
  return t('Release: @title', array('@title' => $title));
}

/**
 * Render the create drupdates release.
 */
function drupdates_release_add($bundle) {
  $args = array('bundle' => $bundle);

  $drupdates_release = entity_create('drupdates_release', $args);

  return drupal_get_form('drupdates_release_edit_form', $drupdates_release);
}

/**
 * Display the edit form.
 */
function drupdates_release_edit(DrupdatesRelease $drupdates_release) {
  return drupal_get_form('drupdates_release_edit_form', $drupdates_release);
}

/**
 * Generate the edit form.
 */
function drupdates_release_edit_form($form, $form_state, $drupdates_release) {
  $form['bundle'] = array(
    '#type' => 'value',
    '#value' => $drupdates_release->bundle,
  );

  $form['default_revision'] = array(
    '#type' => 'value',
    '#value' => $drupdates_release->default_revision,
  );

  if (!empty($drupdates_release->drid)) {
    $form['drid'] = array(
      '#type' => 'value',
      '#value' => $drupdates_release->drid,
    );
    $form['vid'] = array(
      '#type' => 'value',
      '#value' => $drupdates_release->vid,
    );
  }
  else {
    $form['is_new'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );
  }

  $core_options = variable_get("drupdates_release_core_{$drupdates_release->bundle}", "6\r\n7\r\n8");
  $core_options = explode("\r\n", $core_options);
  $form['core'] = array(
    '#title' => t('Core version'),
    '#type' => 'select',
    '#options' => array_combine($core_options, $core_options),
    '#default_value' => $drupdates_release->core,
    '#required' => TRUE,
  );

  if (isset($_GET['dpid'])) {
    $form['dpid'] = array(
      '#type' => 'value',
      '#value' => (int) $_GET['dpid'],
    );
  }
  else {
    $form['dpid'] = array(
      '#title' => t('Project list'),
      '#type' => 'select',
      '#options' => drupdates_project_list(),
      '#default_value' => $drupdates_release->dpid,
      '#required' => TRUE,
      '#maxlength' => 100,
    );
  }

  $form['version'] = array(
    '#title' => t('Version'),
    '#type' => 'textfield',
    '#default_value' => $drupdates_release->version,
    '#required' => TRUE,
    '#maxlength' => 7,
  );

  $form['patch'] = array(
    '#title' => t('Patch level'),
    '#type' => 'textfield',
    '#default_value' => $drupdates_release->patch,
    '#required' => TRUE,
    '#maxlength' => 8,
  );

  $form['release_notes'] = array(
    '#type' => 'text_format',
    '#title' => t('Release notes'),
    '#default_value' => $drupdates_release->release_notes,
    '#format' => $drupdates_release->release_notes_format,
    '#required' => TRUE,
  );

  $form['security'] = array(
    '#title' => t('Security release'),
    '#type' => 'checkbox',
    '#default_value' => $drupdates_release->security,
  );

  $form['targz_fid'] = array(
    '#title' => t('Tar.gz file'),
    '#type' => 'managed_file',
    '#progress_message' => t('Please wait...'),
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('tar.gz'),
    ),
    '#upload_location' => variable_get("drupdates_release_files", 'private://release'),
    '#default_value' => $drupdates_release->targz_fid,
    '#required' => FALSE,
  );

  $form['targz_md5'] = array(
    '#title' => t('Tar.gz md5 file'),
    '#type' => 'text',
    '#default_value' => $drupdates_release->targz_md5,
    '#required' => FALSE,
  );

  $form['zip_fid'] = array(
    '#title' => t('Zip file'),
    '#type' => 'managed_file',
    '#progress_message' => t('Please wait...'),
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('zip'),
    ),
    '#upload_location' => variable_get("drupdates_release_files", 'private://release'),
    '#default_value' => $drupdates_release->zip_fid,
    '#required' => FALSE,
  );

  $form['zip_md5'] = array(
    '#title' => t('Zip md5 file'),
    '#type' => 'text',
    '#default_value' => $drupdates_release->zip_md5,
    '#required' => FALSE,
  );

  $form['active'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#default_value' => $drupdates_release->active,
    '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
    '#required' => TRUE,
  );

  field_attach_form('drupdates_release', $drupdates_release, $form, $form_state);

  $form['actions'] = array(
    '#weight' => 1000,
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  if (!empty($drupdates_release->drid) && (user_access("delete $drupdates_release->bundle release") || user_access("delete any release"))) {
    $entity_uri = entity_uri('drupdates_release', $drupdates_release);
    $form['actions']['delete'] = array(
      '#type' => 'link',
      '#title' => t('Delete'),
      '#href' => $entity_uri['path'] . '/delete',
    );
  }

  // Allow the file upload for tar.zip extentions.
  variable_set('allow_insecure_uploads', TRUE);

  return $form;
}

/**
 * Attach the form validation on release form.
 */
function drupdates_release_edit_form_validate($form, &$form_state) {

  $release_version = $form_state['values']['version'];
  if (!preg_match('/^([0-9]{1,2}(\.)[0-9]{1,2})$/', $release_version)) {
    form_set_error('version', t("The Version field must be decimal."));
  }

  $drupdates_release = (object) $form_state['values'];
  field_attach_form_validate('drupdates_release', $drupdates_release, $form, $form_state);
}

/**
 * Handles submission of the release edit form.
 */
function drupdates_release_edit_form_submit($form, &$form_state) {
  $drupdates_release = reset($form_state['build_info']['args']);

  $file_fields = array('targz', 'zip');
  foreach ($file_fields as $file_type) {
    $file_field = $file_type . '_fid';

    $fid = $form_state['values'][$file_field];

    if (!empty($fid)) {
      $file_entity = file_load($fid);
      if ($file_entity) {
        $form_state['values'][$file_type . '_md5'] = md5_file(drupal_realpath($file_entity->uri));

        // Change status to permanent.
        $file_entity->status = FILE_STATUS_PERMANENT;

        file_save($file_entity);

        file_usage_add($file_entity, 'drupdates_release', 'file', $file_entity->fid);
      }
    }
    elseif (!empty($drupdates_release->{$file_field})) {
      $form_state['values'][$file_type . '_md5'] = '';
      $file_entity = file_load($drupdates_release->{$file_field});
      if ($file_entity) {
        file_usage_delete($file_entity, 'drupdates_release', 'file', $file_entity->fid);
        file_delete($file_entity);
      }
    }
  }

  $drupdates_release = (object) $form_state['values'];
  field_attach_submit('drupdates_release', $drupdates_release, $form, $form_state);
  $saved_release = drupdates_release_save($drupdates_release);
  $form_state['redirect'] = "project/$saved_release->dpid/releases";

  // Moved back to FALSE the insecure variable.
  variable_set('allow_insecure_uploads', FALSE);
}

/**
 * Saves an drupdates release.
 */
function drupdates_release_save($drupdates_release) {
  global $user;

  $release_notes = $drupdates_release->release_notes;
  $drupdates_release->release_notes = $release_notes['value'];
  $drupdates_release->release_notes_format = $release_notes['format'];

  $drupdates_release->modified_uid = $user->uid;
  $drupdates_release->modified = REQUEST_TIME;
  if (!empty($drupdates_release->is_new)) {
    $drupdates_release->created = REQUEST_TIME;
    $drupdates_release->created_uid = $user->uid;
  }
  else {
    $drupdates_release->revision = TRUE;
  }

  entity_save('drupdates_release', $drupdates_release);

  return $drupdates_release;
}

/**
 * Form for deleting drupdates release.
 */
function drupdates_release_delete_form($form, $form_state, $drupdates_release) {
  $entity_uri = entity_uri('drupdates_release', $drupdates_release);
  $title = entity_label('drupdates_release', $drupdates_release);
  return confirm_form(
    $form,
    t("Are you sure you want to delete <strong>@title</strong>?", array('@title' => $title)),
    $entity_uri['path'] . '/view',
    t("Deleting this release will trigger the deletion of all associated resources and cannot be undone."),
    t("Delete")
  );
}

/**
 * Submit handler drupdates_release_delete_form.
 */
function drupdates_release_delete_form_submit($form, &$form_state) {
  $drupdates_release = reset($form_state['build_info']['args']);
  $title = entity_label('drupdates_release', $drupdates_release);
  drupdates_release_delete($drupdates_release);
  drupal_set_message(t("Project '@title' was successfully deleted.", array('@title' => $title)));
  $form_state['redirect'] = 'admin/content/release';
}

/**
 * Remove the drupdates release.
 *
 * @param object $drupdates_release
 *   The drupdates release entity.
 */
function drupdates_release_delete($drupdates_release) {
  $file_fields = array('targz_fid', 'zip_fid');
  foreach ($file_fields as $file_field) {
    $file_entity = file_load($drupdates_release->{$file_field});
    if ($file_entity) {
      file_usage_delete($file_entity, 'drupdates_release', 'file', $file_entity->fid);
      file_delete($file_entity);
    }
  }

  entity_delete('drupdates_release', $drupdates_release->drid);
}

/**
 * Load drupdates release object.
 */
function drupdates_release_load($drid, $reset = FALSE) {
  return entity_load_single('drupdates_release', $drid);
}

/**
 * Loads one or more drupdates release objects.
 */
function drupdates_release_load_multiple($drid = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('drupdates_release', $drid, $conditions, $reset);
}

/**
 * Checks if a release type bundle exists.
 *
 * @param text $name
 *   The bundle to look up.
 *
 * @return bool
 *   Does the bundle exist?
 */
function drupdates_release_type_bundle_exists($name) {
  $bundles = drupdates_release_type_load_bundles();
  return isset($bundles[$name]);
}

/**
 * Returns all defined release type bundles.
 *
 * @return array
 *   All release type bundles.
 */
function drupdates_release_type_load_bundles() {
  $bundles = &drupal_static(__FUNCTION__);

  if (!isset($bundles)) {
    $bundles = db_select('drupdates_release_type', 'drt')
      ->fields('drt')
      ->orderBy('bundle')
      ->execute()
      ->fetchAllAssoc('bundle');
  }

  return $bundles;
}

/**
 * List drupdates release types that can be created.
 */
function drupdates_release_page_add() {
  $bundles = drupdates_release_type_load_bundles();

  switch (count($bundles)) {
    case 1:
      $bundle = array_shift($bundles);
      drupal_goto("release/add/{$bundle->bundle}");
      break;

    case 0:
      return '<p>' . t('You have not created any release types yet.') . '</p>';

    default:
      return theme('drupdates_release_add_list', array('content' => $bundles));
  }
}

/**
 * Title callback function for edit property page.
 */
function drupdates_release_title_edit_property($bundle, $field_name) {
  return t("Edit @field property value for @bundle", array('@field' => $field_name, '@bundle' => $bundle));
}

/**
 * Edit release property form.
 */
function drupdates_release_property_edit_form($form, $form_state, $bundle, $property) {
  $release_entity = entity_get_property_info('drupdates_release');

  $property_title = $release_entity['properties'][$property]['label'];

  $property_value = variable_get("drupdates_release_{$property}_{$bundle}", "6\r\n7\r\n8");

  $form[$property] = array(
    '#title' => check_plain($property_title),
    '#type' => 'textarea',
    '#default_value' => $property_value,
    '#required' => TRUE,
  );

  $form['bundle'] = array(
    '#type' => 'value',
    '#value' => $bundle,
  );

  $form['property'] = array(
    '#type' => 'value',
    '#value' => $property,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate handler for release property edit form.
 */
function drupdates_release_property_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $property = $values['property'];

  $property_values = explode("\r\n", $values[$property]);

  foreach ($property_values as &$property_value) {
    if (!is_numeric($property_value) || intval($property_value) != $property_value || $property_value <= 0) {
      form_set_error($property, "Only whole numbers are allowed.");
      break;
    }
  }
}

/**
 * Submit handler for release property edit form.
 */
function drupdates_release_property_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $bundle = $values['bundle'];
  $property = $values['property'];

  variable_set("drupdates_release_{$property}_{$bundle}", $values[$property]);

  drupal_set_message(t('Saved @property property configuration.', array('@property' => $property)));
}
