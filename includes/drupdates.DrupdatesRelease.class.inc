<?php

/**
 * @file
 * DrupdatesRelease
 */

/**
 * DrupdatesRelease class.
 */
class DrupdatesRelease extends Entity {

  public $is_new = FALSE;

  public $drid = 0;

  public $dpid;

  public $vid = 0;

  public $uuid;

  public $vuuid;

  public $bundle;

  public $core;

  public $version;

  public $patch;

  public $release_notes;

  public $release_notes_format;

  public $security;

  public $targz_fid;

  public $targz_md5;

  public $zip_fid;

  public $zip_md5;

  public $active = TRUE;

  public $created;

  public $created_uid;

  public $modified;

  public $modified_uid;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'drupdates_release');
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {

    $content = array();

    $content['drid'] = array(
      '#theme' => 'field',
      '#title' => t('DRID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->drid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->drid),
    );

    $content['dpid'] = array(
      '#theme' => 'field',
      '#title' => t('DPID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->dpid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->dpid),
    );

    $content['vid'] = array(
      '#theme' => 'field',
      '#title' => t('Revision ID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->vid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->vid),
    );

    $content['uuid'] = array(
      '#theme' => 'field',
      '#title' => t('UUID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->uuid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->uuid),
    );

    $content['vuuid'] = array(
      '#theme' => 'field',
      '#title' => t('Revision UUID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->vuuid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->vuuid),
    );

    $content['bundle'] = array(
      '#theme' => 'field',
      '#title' => t('Release Type'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->bundle)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->bundle),
    );

    $content['core'] = array(
      '#theme' => 'field',
      '#title' => t('Core Version'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->core)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->core),
    );

    $content['version'] = array(
      '#theme' => 'field',
      '#title' => t('Version'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->version)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->version),
    );

    $content['patch'] = array(
      '#theme' => 'field',
      '#title' => t('Patch Level'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->patch)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->patch),
    );

    $content['release_notes'] = array(
      '#theme' => 'field',
      '#title' => t('Release Notes'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->release_notes)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->release_notes),
    );

    $content['release_notes_format'] = array(
      '#theme' => 'field',
      '#title' => t('Release Notes Format'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->release_notes_format)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->release_notes_format),
    );

    $security_str = $this->security ? t('Yes') : t('No');
    $content['security'] = array(
      '#theme' => 'field',
      '#title' => t('Security Release'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $security_str)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $security_str),
    );

    $targz_entity = file_load($this->targz_fid);
    if ($targz_entity) {
      $content['targz_fid'] = array(
        '#theme' => 'field',
        '#title' => t('Tar.gz file'),
        '#access' => TRUE,
        '#view_mode' => $view_mode,
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_description',
        '#field_type' => 'text',
        '#label_display' => 'none',
        '#entity_type' => 'drupdates_release',
        '#bundle' => $this->bundle,
        '#items' => array(array('value' => $targz_entity->filename)),
        '#formatter' => 'text_default',
        0 => array('#markup' => $targz_entity->filename),
      );
    }

    $content['targz_md5'] = array(
      '#theme' => 'field',
      '#title' => t('Tar.gz md5 file'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->targz_md5)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->targz_md5),
    );

    $zip_entity = file_load($this->zip_fid);
    if ($zip_entity) {
      $content['zip_fid'] = array(
        '#theme' => 'field',
        '#title' => t('Zip file'),
        '#access' => TRUE,
        '#view_mode' => $view_mode,
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_description',
        '#field_type' => 'text',
        '#label_display' => 'none',
        '#entity_type' => 'drupdates_release',
        '#bundle' => $this->bundle,
        '#items' => array(array('value' => $zip_entity->filename)),
        '#formatter' => 'text_default',
        0 => array('#markup' => $zip_entity->filename),
      );
    }

    $content['zip_md5'] = array(
      '#theme' => 'field',
      '#title' => t('Zip md5 file'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->zip_md5)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->zip_md5),
    );

    $active_str = $this->active ? t('Yes') : t('No');
    $content['active'] = array(
      '#theme' => 'field',
      '#title' => t('Active'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $active_str)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $active_str),
    );

    $created = format_date($this->created);
    $content['created'] = array(
      '#theme' => 'field',
      '#title' => t('Created'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array($created),
      '#formatter' => 'text_default',
      0 => array('#markup' => $created),
    );

    $user_name = entity_label('user', user_load($this->created_uid));
    $content['created_uid'] = array(
      '#theme' => 'field',
      '#title' => t('Created User'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $user_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => l($user_name, 'user/' . $this->created_uid)),
    );

    $modified = format_date($this->modified);
    $content['modified'] = array(
      '#theme' => 'field',
      '#title' => t('Last Modified'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array($modified),
      '#formatter' => 'text_default',
      0 => array('#markup' => $modified),
    );

    $user_name = entity_label('user', user_load($this->modified_uid));
    $content['modified_uid'] = array(
      '#theme' => 'field',
      '#title' => t('Last Modified User'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_release',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $user_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => l($user_name, 'user/' . $this->modified_uid)),
    );

    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultUri() {
    return array('path' => "project/$this->dpid/release/$this->drid");
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (isset($this->core) && isset($this->version) && isset($this->patch)) {
      return "{$this->core}.x-{$this->version}.{$this->patch}";
    }
    return t('Invalid');
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    global $user;
    $this->modified_uid = $user->uid;

    $this->modified = REQUEST_TIME;
    if (!empty($this->is_new)) {
      $this->created = REQUEST_TIME;
    }
    else {
      $this->revision = TRUE;
    }

    parent::save();
  }
}
