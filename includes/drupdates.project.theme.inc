<?php

/**
 * @file
 * Theme hook implementations for drupdates_project entity.
 */

/**
 * Returns HTML for a list of available project types for creation.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of project types.
 *
 * @ingroup themeable
 */
function theme_drupdates_project_add_list($variables) {
  $items = array();

  foreach ($variables['content'] as $item) {
    $items[] = array(
      'data' => l($item->label, 'project/add/' . $item->bundle),
    );
  }

  $output = array(
    'items' => $items,
    'title' => 'Project types',
    'type' => 'ul',
    'attributes' => array('class' => 'project-type-list admin-list'),
  );

  return theme_item_list($output);
}
