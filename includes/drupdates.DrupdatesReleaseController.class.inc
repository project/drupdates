<?php

/**
 * @file
 * DrupdatesReleaseController controller.
 */

/**
 * DrupdatesReleaseController controller class.
 */
class DrupdatesReleaseController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $values += array(
      'active' => TRUE,
      'is_new' => TRUE,
      'default_revision' => TRUE,
    );

    return parent::create($values);
  }
}
