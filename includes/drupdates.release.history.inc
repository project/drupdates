<?php

/**
 * @file
 * Drupdates release history functions.
 */

/**
 * Renders the release history for the specified project.
 */
function drupdates_release_history($machine_name, $core) {
  global $base_url;

  $xml = new DOMDocument();
  $xml->formatOutput = TRUE;
  drupal_add_http_header('Content-Type', 'text/xml');

  // Load the project with related machine name.
  $project = drupdates_project_load_multiple(FALSE, array('machine_name' => $machine_name));

  if (empty($project)) {
    $error = $xml->appendChild($xml->createElement("error"));
    $error->nodeValue = "No project was found for the requested machine name ({$machine_name})";
    print $xml->saveXML();
    drupal_exit();
  }

  $project = reset($project);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'drupdates_release')
    ->propertyCondition('dpid', $project->dpid)
    ->propertyCondition('core', $core);
  $result = $query->execute();

  if (!isset($result['drupdates_release'])) {
    $error = $xml->appendChild($xml->createElement("error"));
    $error->nodeValue = "No release history was found for the requested project ({$project->project})";
    print $xml->saveXML();
    drupal_exit();
  }

  $release_drids = array_keys($result['drupdates_release']);

  // Load all the releases related to project.
  $project_releases = drupdates_release_load_multiple($release_drids);

  $root = $xml->appendChild($xml->createElement("project"));
  $value = $xml->createTextNode('http://purl.org/dc/elements/1.1/');
  $xmlns = $xml->createAttribute('xmlns:dc');
  $xmlns->appendChild($value);
  $root->appendChild($xmlns);

  $project_status = ($project->active == 1) ? 'published' : 'unpublished';
  $dc_creator = user_load($project->created_uid);

  $root->appendChild($xml->createElement('title', $project->project));
  $root->appendChild($xml->createElement('short_name', $project->machine_name));
  $root->appendChild($xml->createElement('dc:creator', $dc_creator->name));
  $root->appendChild($xml->createElement('type', $project->bundle));
  $root->appendChild($xml->createElement('api_version', 'TBD'));
  $root->appendChild($xml->createElement('recommended_major', 'TBD'));
  $root->appendChild($xml->createElement('supported_majors', 'TBD'));
  $root->appendChild($xml->createElement('default_major', 'TBD'));
  $root->appendChild($xml->createElement('project_status', $project_status));
  $root->appendChild($xml->createElement('link', "$base_url/project/$project->dpid"));

  $project_terms = $root->appendChild($xml->createElement("terms"));
  $project_term = $project_terms->appendChild($xml->createElement("term"));

  foreach (drupdates_project_type_load_bundles() as $project_bundle) {
    $project_term->appendChild($xml->createElement('name', 'Projects'));
    $project_term->appendChild($xml->createElement('value', $project_bundle->label));
  }

  $releases = $root->appendChild($xml->createElement("releases"));

  foreach ($project_releases as $project_release) {
    $release_status = ($project_release->active == 1) ? 'published' : 'unpublished';
    $release_link = "$base_url/project/$project_release->dpid/release/$project_release->drid";
    $release_name = $project->machine_name . ' ' . $project_release->version;

    $targz_entity = file_load($project_release->targz_fid);
    $targz_url = file_create_url($targz_entity->uri);
    $targz_url = url($targz_url, array('absolute' => TRUE));
    $download_url = $targz_url;
    $targz_md5 = md5_file($targz_url);

    $zip_entity = file_load($project_release->zip_fid);
    $zip_url = file_create_url($zip_entity->uri);
    $zip_url = url($zip_url, array('absolute' => TRUE));
    $zip_md5 = md5_file($zip_url);

    drupal_alter('drupdates_download_url', $download_url, $project, $project_release);
    drupal_alter('drupdates_targz_url', $targz_url, $project, $project_release);
    drupal_alter('drupdates_zip_url', $zip_url, $project, $project_release);

    $release = $releases->appendChild($xml->createElement("release"));

    $release->appendChild($xml->createElement('name', $release_name));
    $release->appendChild($xml->createElement('version', $project_release->version));
    $release->appendChild($xml->createElement('tag', 'TBD'));
    $release->appendChild($xml->createElement('version_major', $project_release->version));
    $release->appendChild($xml->createElement('version_patch', $project_release->patch));
    $release->appendChild($xml->createElement('status', $release_status));
    $release->appendChild($xml->createElement('release_link', $release_link));
    $release->appendChild($xml->createElement('download_link', $targz_url));
    $release->appendChild($xml->createElement('date', $project_release->created));
    $release->appendChild($xml->createElement('mdhash', $targz_md5));
    $release->appendChild($xml->createElement('filesize', $targz_entity->filesize));

    $files = $release->appendChild($xml->createElement("files"));

    $targz_file = $files->appendChild($xml->createElement('file'));
    $targz_file->appendChild($xml->createElement('url', $targz_url));
    $targz_file->appendChild($xml->createElement('archive_type', 'tar.gz'));
    $targz_file->appendChild($xml->createElement('md5', $targz_md5));
    $targz_file->appendChild($xml->createElement('size', $targz_entity->filesize));
    $targz_file->appendChild($xml->createElement('filedate', $targz_entity->timestamp));

    $zip_file = $files->appendChild($xml->createElement('file'));
    $zip_file->appendChild($xml->createElement('url', $zip_url));
    $zip_file->appendChild($xml->createElement('archive_type', 'zip'));
    $zip_file->appendChild($xml->createElement('md5', $zip_md5));
    $zip_file->appendChild($xml->createElement('size', $zip_entity->filesize));
    $zip_file->appendChild($xml->createElement('filedate', $zip_entity->timestamp));

    if ($project_release->security == 1) {
      $release_terms = $release->appendChild($xml->createElement("terms"));
      $release_term = $release_terms->appendChild($xml->createElement("term"));

      $release_term->appendChild($xml->createElement('name', 'Release type'));
      $release_term->appendChild($xml->createElement('value', 'Security update'));
    }
  }

  print $xml->saveXML();

  drupal_exit();
}
