<?php

/**
 * @file
 * DrupdatesProjectController controller.
 */

/**
 * DrupdatesProjectController controller class.
 */
class DrupdatesProjectController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $values += array(
      'active' => TRUE,
      'is_new' => TRUE,
      'default_revision' => TRUE,
    );

    return parent::create($values);
  }
}
