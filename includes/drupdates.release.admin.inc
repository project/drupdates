<?php

/**
 * @file
 * Drupdates admin release functions.
 */

/**
 * Generates the drupdates release type edit form.
 */
function drupdates_release_type_form($form, &$form_state, $release_type, $op = 'edit') {

  if ($op == 'clone') {
    $release_type->label .= ' (cloned)';
    $release_type->bundle .= '_clone';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => !empty($release_type->label) ? $release_type->label : '',
  );

  $form['bundle'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($release_type->bundle) ? $release_type->bundle : '',
    '#disabled' => empty($release_type->is_new),
    '#machine_name' => array(
      'exists' => 'drupdates_release_type_bundle_exists',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this drupdates release type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save release type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function drupdates_release_type_form_submit(&$form, &$form_state) {
  $release_type = entity_ui_form_submit_build_entity($form, $form_state);
  $release_type->save();
  $form_state['redirect'] = 'admin/structure/release-types';
}
