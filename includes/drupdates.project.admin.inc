<?php

/**
 * @file
 * Drupdates admin project functions.
 */

/**
 * Generates the drupdates project type edit form.
 */
function drupdates_project_type_form($form, &$form_state, $project_type, $op = 'edit') {

  if ($op == 'clone') {
    $project_type->label .= ' (cloned)';
    $project_type->bundle .= '_clone';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => !empty($project_type->label) ? $project_type->label : '',
  );

  $form['bundle'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($project_type->bundle) ? $project_type->bundle : '',
    '#disabled' => empty($project_type->is_new),
    '#machine_name' => array(
      'exists' => 'drupdates_project_type_bundle_exists',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this drupdates project type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save project type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function drupdates_project_type_form_submit(&$form, &$form_state) {
  $project_type = entity_ui_form_submit_build_entity($form, $form_state);
  $project_type->save();
  $form_state['redirect'] = 'admin/structure/project-types';
}
