<?php

/**
 * @file
 * DrupdatesProject
 */

/**
 * DrupdatesProject class.
 */
class DrupdatesProject extends Entity {

  public $is_new = FALSE;

  public $dpid = 0;

  public $vid = 0;

  public $uuid;

  public $vuuid;

  public $bundle;

  public $project;

  public $machine_name;

  public $description;

  public $description_format;

  public $active = TRUE;

  public $created;

  public $created_uid;

  public $modified;

  public $modified_uid;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'drupdates_project');
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {

    $content = array();

    $content['dpid'] = array(
      '#theme' => 'field',
      '#title' => t('DPID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->dpid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->dpid),
    );

    $content['vid'] = array(
      '#theme' => 'field',
      '#title' => t('Revision ID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->vid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->vid),
    );

    $content['uuid'] = array(
      '#theme' => 'field',
      '#title' => t('UUID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->uuid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->uuid),
    );

    $content['vuuid'] = array(
      '#theme' => 'field',
      '#title' => t('Revision UUID'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->vuuid)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->vuuid),
    );

    $content['bundle'] = array(
      '#theme' => 'field',
      '#title' => t('Project Type'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->bundle)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->bundle),
    );

    $content['project'] = array(
      '#theme' => 'field',
      '#title' => t('Project Name'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->project)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->project),
    );

    $content['machine_name'] = array(
      '#theme' => 'field',
      '#title' => t('Machine Name'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->machine_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->machine_name),
    );

    $content['description'] = array(
      '#theme' => 'field',
      '#title' => t('Description'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->description)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->description),
    );

    $content['description_format'] = array(
      '#theme' => 'field',
      '#title' => t('Description Format'),
      '#access' => FALSE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $this->description_format)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $this->description_format),
    );

    $active_str = $this->active ? t('Yes') : t('No');
    $content['active'] = array(
      '#theme' => 'field',
      '#title' => t('Active'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'none',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $active_str)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $active_str),
    );

    $created = format_date($this->created);
    $content['created'] = array(
      '#theme' => 'field',
      '#title' => t('Created'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array($created),
      '#formatter' => 'text_default',
      0 => array('#markup' => $created),
    );

    $user_name = entity_label('user', user_load($this->created_uid));
    $content['created_uid'] = array(
      '#theme' => 'field',
      '#title' => t('Created User'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $user_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => l($user_name, 'user/' . $this->created_uid)),
    );

    $modified = format_date($this->modified);
    $content['modified'] = array(
      '#theme' => 'field',
      '#title' => t('Last Modified'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array($modified),
      '#formatter' => 'text_default',
      0 => array('#markup' => $modified),
    );

    $user_name = entity_label('user', user_load($this->modified_uid));
    $content['modified_uid'] = array(
      '#theme' => 'field',
      '#title' => t('Last Modified User'),
      '#access' => TRUE,
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#label_display' => 'above',
      '#entity_type' => 'drupdates_project',
      '#bundle' => $this->bundle,
      '#items' => array(array('value' => $user_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => l($user_name, 'user/' . $this->modified_uid)),
    );

    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultUri() {
    return array('path' => 'project/' . $this->identifier());
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = t('Invalid');
    if (isset($this->project)) {
      return $this->project;
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    global $user;
    $this->modified_uid = $user->uid;

    $this->modified = REQUEST_TIME;
    if (!empty($this->is_new)) {
      $this->created = REQUEST_TIME;
    }
    else {
      $this->revision = TRUE;
    }

    parent::save();
  }
}
