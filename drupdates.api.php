<?php

/**
 * @file
 * Drupdates module API functions.
 */

/**
 * Alter the download URL for a drupdates release file.
 *
 * @param string $download_url
 *   The original download URL.
 * @param stdClass $project
 *   The drupdates project the download relates to.
 * @param stdClass $release
 *   The release the download relates to.
 */
function hook_drupdates_download_url_alter(&$download_url, $project, $release) {
  $file = entity_load('file', array($release->targz_fid));
  if ('gz' != strtolower(substr($download_url, -2))) {
    $file = entity_load('file', array($release->zip_fid));
  }

  $file = reset($file);
  $download_url = "https://github.com/example/{$project->machine_name}/{$file->filename}";
}
